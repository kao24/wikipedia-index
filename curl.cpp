/*
    Derived from: http://www.cplusplus.com/forum/unices/45878/#msg249201
    Referenced May 20, 2013

    This defines curlRetrieve() and its supporting functions, which retrieve
    the HTML of a parameterized URL. Based on another boolean input,
    curlRetrieve() will search for a link to the next page of lists of
    Wikipedia articles. If this link is searched for and found, it will
    be URLDecoded using libCurl's escape function and then passed through
    a regular expression that replaces &amp; with &. The results are
    available in global variables (for index.cpp to use).
*/

#ifndef __CURLRETRIEVE__
#define __CURLRETRIEVE__

#include <curl/curl.h>
#include <string>
#include <boost/regex.hpp>
using namespace std;

string curlData;
string nextPage = "a"; // some non-empty value to initialize the scrape loop
boost::regex nextRegex("\\| <a href=\"([^ ]+)\"([^>])*>Next page ");
boost::regex ampRegex("\\&amp;");
boost::smatch matches;

CURL * curlObject;

size_t writeCallback(char * buf, size_t size, size_t nmemb, void * up)
{
    //callback must have this declaration
    //buf is a pointer to the data that curl has for us
    //size*nmemb is the size of the buffer

    for (size_t i=0; i<size*nmemb; ++i)
        curlData.push_back(buf[i]);

    return size*nmemb;  // return how many bytes have been handled
}

void curlInitialize() {
    curl_global_init(CURL_GLOBAL_NOTHING);
    curlObject = curl_easy_init();

    curl_easy_setopt(curlObject, CURLOPT_WRITEFUNCTION, writeCallback);
    curl_easy_setopt(curlObject, CURLOPT_FOLLOWLOCATION, 1);
}

void curlClear() {
    curl_easy_cleanup(curlObject);
    curl_global_cleanup();
}

void curlRetrieve(const char * url, bool findArticles = false)
{
    curlData.clear();
    if (findArticles)
        nextPage.clear();

    curl_easy_setopt(curlObject, CURLOPT_URL, url);

    curl_easy_perform(curlObject);

    // obtain the next page, if it's available
    if (findArticles && boost::regex_search(curlData, matches, nextRegex))
    {
        char * decode = curl_easy_unescape(curlObject, matches[1].str().data(), 0, NULL);
        nextPage = decode;
        curl_free(decode);
        nextPage = boost::regex_replace(nextPage, ampRegex, "&");
    }
}

#endif
