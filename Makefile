COMPILE = g++ -c -g -O2 -Wall -Werror
LINK = g++
INDEXFILES = index.cpp curl.cpp
SEARCHFILES = search.cpp

all: search index

search: search.o
	$(LINK) search.o -o search `xapian-config --libs`

search.o: search.cpp
	$(COMPILE) $(SEARCHFILES)

index: index.o
	$(LINK) index.o -o index -lcurl -lboost_regex-mt `xapian-config --libs`

index.o: $(INDEXFILES)
	$(COMPILE) $(INDEXFILES)

clean:
	rm -r -f *.o

cleandb:
	rm -r -f wikiDatabase
