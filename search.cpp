/*
    This file is taken almost entirely from Xapian's quickstart tutorial
    available here: http://xapian.org/docs/quickstart.html
    Last referenced May 20, 2013

    This program takes every discrete input word as a search query to
    the database specified in the file, stub_db. The database then
    returns a set of "best results," which is then printed to the console.

    The search is currently case-sensitive
    (i.e. a search for "rose" will ignore the keyword "Rose").
    Here, it is assumed that the primary keyword matching the title
    of an article will be used several times such that capitalization
    caused by the keyword starting a sentence is compensated by
    repetition of the same keyword elsewhere, such that it is not
    capitalized if it is not a proper noun.
*/

#include <xapian.h>
#include <iostream>
using namespace std;

int main(int argc, char **argv)
{
    // Simplest possible options parsing: we just require two or more
    // parameters.
    if (argc < 2) {
        cout << "usage: " << argv[0] <<
                " <search terms>" << endl;
        return 1;
    }

    // Catch any Xapian::Error exceptions thrown
    try {
        // Make the database
        Xapian::Database db("stub_db");

        // Start an enquire session
        Xapian::Enquire enquire(db);

        // Build the query object
        Xapian::Query query(Xapian::Query::OP_OR, argv + 1, argv + argc);
        cout << "Performing query `" << query.get_description() << "'" << endl;

        // Give the query object to the enquire session
        enquire.set_query(query);

        // Get the top 10 results of the query
        Xapian::MSet matches = enquire.get_mset(0, 10);

        // Display the results
        cout << matches.size() << " results found" << endl;

        for (Xapian::MSetIterator i = matches.begin();
             i != matches.end();
             ++i) {
            Xapian::Document doc = i.get_document();
            cout << "Document ID " << *i << "\t" <<
                    i.get_percent() << "% [" <<
                    doc.get_data() << "]" << endl;
        }
    } catch(const Xapian::Error &error) {
        cout << "Exception: "  << error.get_msg() << endl;
    }
}
