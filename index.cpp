/*
    Crawls en.wikipedia.org to find all articles available (in English),
    curls each article, removes HTML tags and punctuation for purely text,
    which can be treated as a verbose set of keywords, and then uses
    the entirety of this text to index each article's link/title into a
    Xapian::Chert database as specified in the file, stub_db.
*/

#ifndef __SCRAPE__
#define __SCRAPE__

#include <iostream>
#include <string>
#include <utility>
#include <algorithm>

#include <boost/regex.hpp>
#include <xapian.h>

#include "curl.cpp"
using namespace std;

static const string urlBase = "http://en.wikipedia.org";
static const string urlBaseMobile = "http://en.m.wikipedia.org";

// regex for HTML tags, whitespace, common English punctuation/semantics
// note that the clause for ";" will affect HTML-escaped characters (e.g. &amp;)
static const boost::regex removeRegex("((<[^>]+>)|\\h|\\v|\\.|,|\\!|\\?|-{2}|\\(|\\)|\\[|\\]|/|\"|:|;)+");

// regex to search for each article and its corresponding title
boost::regex articleRegex("<a href=\"(/wiki/.+?)\".*? title=\"(.+?)\">");

// arbitrarily-defined function to combine two strings into one
string stringify(string location, string title) {
    return location + " :: " + title;
}

// inverse function of stringify()
void destringify(string stringified, string (& result)[2]) {
    size_t locationEnd = stringified.find(" :: ");
    result[0] = stringified.substr(0, locationEnd);
    result[1] = stringified.substr(locationEnd+4);
}

void xapianIndex(Xapian::WritableDatabase & db,
                    std::list< std::pair<string, string> > & linked) {

    string curlTarget, location, title;
    size_t findStart, findNotes, findEnd, findSpace;

    while (!linked.empty()) {
        // set aliases for the std::pair attributes
        location = linked.front().first;
        title = linked.front().second;

        curlTarget = urlBaseMobile + location;  // location is relative to domain

        curlRetrieve(curlTarget.data());  // the article html is now in curlData

        // remove irrelevant HTML wrappings, keep only the text relating to the article.
        // NOTE: these string parameters for find() are highly volatile and
        // may change, depending on Wikipedia.
        findStart = curlData.find("class=\"content\"") + 16;
        findNotes = curlData.find("<span id=\"Notes\">");
        findEnd = curlData.find("<span id=\"References\">");
        findEnd = std::min(findEnd, findNotes);
        curlData = curlData.substr(findStart, findEnd - findStart);

        // remove HTML tags and extraneous whitespaces
        curlData = boost::regex_replace(curlData, removeRegex, " ");

        try
        {
            Xapian::Document doc;
            doc.set_data(stringify(location, title));

            findSpace = curlData.find(" ");
            // remove leading space, if present
            if (findSpace == 0)
            {
                curlData = curlData.substr(1);
                findSpace = curlData.find(" ");
            }

            // add the title of this article (especially important
            // for redirects) as a keyword
            doc.add_term(title);

            // add each space-delimited word as a keyword for this article
            while (findSpace != string::npos)
            {
                doc.add_term(curlData.substr(0, findSpace));
                curlData = curlData.substr(findSpace+1);
                findSpace = curlData.find(" ");
            }

            db.add_document(doc);
        }
        catch(const Xapian::Error &error)
        {
            cout << "Exception: " << error.get_msg() << endl;
        }

        linked.pop_front();
    }
}

int main() {
    time_t timer = time(NULL);
    curlInitialize();

    string page = urlBase;
    page.append("/w/index.php?title=Special:AllPages&from=!");

    // second-to-last page, for debugging purposes to make sure the loop ends
    //page = "http://en.wikipedia.org/w/index.php?title=Special:AllPages&from=%F0%9F%92%A4";

    // declare variable to contain match_results of boost::regex_search()
    boost::smatch articleMatches;
    
    size_t foundIndex;

    size_t numArticles = 0, numPages = 0;

    // use linked list to contain each page's list of articles.
    // Processing articles in these chunks allows us to
    // avoid having to worry about saving the previous curlData
    // in another string for later use. (curlData is overwritten at
    // each call of curlRetrieve().)
    // this should also theoretically use less overall memory than
    // keeping the HTML of the list of articles.
    // runtime may be affected, however. (untested)
    std::list< std::pair<string, string> > linked;
    
    try {
        Xapian::WritableDatabase db("stub_db", Xapian::DB_CREATE_OR_OPEN);

        while (!nextPage.empty())
        {
            cout << "searching page " << ++numPages
                    << " for articles at " << page << endl;
    
            curlRetrieve(page.data(), true);
    
            // prepare the next page url to be scraped
            page = urlBase;
            page.append(nextPage);
    
            // find the wikipedia articles/redirects
            // locate the table that contains all article links
            foundIndex = curlData.find("chunk");
            curlData = curlData.substr(foundIndex+4, curlData.find("<hr")-foundIndex);
    
            // find each article/title pair and index it
            while (boost::regex_search(curlData, articleMatches, articleRegex))
            {
                ++numArticles;
                linked.push_back(std::pair<string, string>(articleMatches[1], articleMatches[2]));
                curlData = curlData.substr(articleMatches.position(1)+1);
            }

            // index the articles found
            xapianIndex(db, linked);

            if (numPages % 3 == 0)
                db.commit();    // commit more often than Xapian library
                                // default of 10,000 changes, in case
                                // the indexer fails mid-sequence.
                                // (interrupt discards un-committed changes.)
                                // it should be noted that more commits
                                // cause slower indexing, according to documentation
        }
    
        db.close();
    } catch(const Xapian::Error &error) {
        cout << "Xapian Exception: " << error.get_msg() << endl;
    }
    curlClear();

    cout << numArticles << " articles found, total" << endl;
    timer = time(NULL) - timer; // calculate total runtime in seconds
    cout << "execution finished in " << timer/60 << ":" << timer%60 << endl;
}

#endif
