This is an attempt to solve a code challenge to provide
search functionality for wikipedia.org. This source code
produces two executables: one that crawls and indexes
en.wikipedia.org, and another that searches the database
produced by the indexer.

The indexer does not take any inputs.
The searcher takes a variable number of inputs, each of which
is queried against the database. The database then returns
a set of results (containing the Wikipedia link and the
corresponding article's name/title) that is calculated by
the Xapian library to be most appropriate. These results
are then printed to the console.

This solution adds all text (excluding HTML) from an article
as keywords to the article, assuming that common English words
(e.g. "a", "an", "the", "he", "she", etc.) will hold very
little weight if used in search queries in conjunction with
more relevant keywords.

The search is currently case-sensitive
(i.e. a search for "rose" will ignore the keyword "Rose").
Here, it is assumed that the primary keyword matching the title
of an article will be used several times such that capitalization
caused by the keyword starting a sentence is compensated by
repetition of the same keyword elsewhere, such that it is not
capitalized if it is not a proper noun. This treatment of
capitalized letters would theoretically make searching for
proper nouns more accurate. For example, if we were searching
for "Rose," the name, as opposed to "rose," the flower.

This source code compiles and runs on linux (Debian 4.7.2-5)
with gcc 4.7.2, Xapian 1.2.12, Boost 1.53.0, libCurl 7.26.0

This source code also compiles with Cygwin, but the Xapian
library is sensitive to the inclusion and usage of the other
libraries, making the indexer not dependable on this platform;
the indexer will sometimes write correctly and sometimes
throw an exception, reporting that a Cygwin library cannot
secure a fork.
